package uz.azn.pdfreader

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import uz.azn.pdfreader.databinding.FragmentReadBookBinding

class ReadBookFragment : Fragment(R.layout.fragment_read_book) {
    private lateinit var binding:FragmentReadBookBinding
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentReadBookBinding.bind(view)
    }

}