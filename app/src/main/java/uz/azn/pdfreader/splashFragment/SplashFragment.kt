package uz.azn.pdfreader.splashFragment

import android.content.Context
import android.os.Binder
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import uz.azn.pdfreader.R
import uz.azn.pdfreader.databinding.FragmentReadBookBinding
import uz.azn.pdfreader.databinding.FragmentSplashBinding
import uz.azn.pdfreader.introFragment.IntroFragment


class SplashFragment(val mcontext:Context) : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        Handler().postDelayed({
            val manager = fragmentManager
            manager?.beginTransaction()?.setCustomAnimations(
                R.anim.enter_from_rigth, R.anim.exit_to_left,
                R.anim.exit_to_left, R.anim.enter_from_rigth
            )?.replace(R.id.splash_frame,IntroFragment(mcontext))?.commit()
        },1000)

        return inflater.inflate(R.layout.fragment_splash, container, false)
    }

}