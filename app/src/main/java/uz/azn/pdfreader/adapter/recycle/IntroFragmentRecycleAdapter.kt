package uz.azn.pdfreader.adapter.recycle

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import uz.azn.pdfreader.R
import uz.azn.pdfreader.ReadBookFragment
import uz.azn.pdfreader.adapter.onItemClick.OnItemClickListener
import uz.azn.pdfreader.databinding.FragmentIntroBinding
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

class IntroFragmentRecycleAdapter() :
    RecyclerView.Adapter<IntroFragmentRecycleAdapter.MyViewHolder>(), Filterable {
    lateinit var onItemClickListener: OnItemClickListener
    var titles: MutableList<File> = arrayListOf()
    var countryFilterList: MutableList<File> = titles

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(file: File) {
            val tvName = itemView.findViewById<TextView>(R.id.tv_name)
            tvName.text = file.name

        }

        init {
            itemView.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    onItemClickListener.onItemClicked(adapterPosition)

                }
            }

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.pdf_items, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int = countryFilterList.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val s = countryFilterList[position]
        holder.bind(s)


    }

    fun addTitle(text: MutableList<File>, isResetNeeded: Boolean = false) {

        if (isResetNeeded) titles.clear()
        titles.addAll(text)
        notifyDataSetChanged()
    }

    fun setOnItemClickedListener(listener: OnItemClickListener) {
        this.onItemClickListener = listener
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                if (charSearch.isEmpty()) {
                    countryFilterList = titles
                } else {
                    val resultList = ArrayList<File>()
                    for (row in titles) {
                        if (row.name.toLowerCase(Locale.ROOT)
                                .contains(charSearch.toLowerCase(Locale.ROOT))
                        ) {
                            resultList.add(row)
                        }
                    }
                    countryFilterList = resultList
                }
                val filterResults = FilterResults()
                filterResults.values = countryFilterList
                return filterResults
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                countryFilterList = results?.values as ArrayList<File>
                notifyDataSetChanged()
            }

        }

    }
}


