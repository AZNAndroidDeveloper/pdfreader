package uz.azn.pdfreader.adapter.fragment

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class FragmentAdapter(fragmentManager: FragmentManager):FragmentPagerAdapter(fragmentManager) {
   val fragment:MutableList<Fragment> = ArrayList()

    override fun getItem(position: Int): Fragment  = fragment[position]

    override fun getCount(): Int = fragment.size

}