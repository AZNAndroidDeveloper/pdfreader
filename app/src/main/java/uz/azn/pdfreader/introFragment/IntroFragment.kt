package uz.azn.pdfreader.introFragment

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.widget.SearchView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.view.MenuItemCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import uz.azn.pdfreader.R
import uz.azn.pdfreader.adapter.onItemClick.OnItemClickListener
import uz.azn.pdfreader.adapter.recycle.IntroFragmentRecycleAdapter
import uz.azn.pdfreader.databinding.FragmentIntroBinding
import uz.azn.pdfreader.pdfViewerFragment.PdfViewerFragment
import java.io.File


class IntroFragment(mContext: Context) : Fragment(R.layout.fragment_intro) {
    lateinit var binding: FragmentIntroBinding

    var fileList: MutableList<File> = ArrayList()
    lateinit var dir: File
    lateinit var introFragmentRecycleAdapter: IntroFragmentRecycleAdapter

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentIntroBinding.bind(view)
        introFragmentRecycleAdapter = IntroFragmentRecycleAdapter()
        getFile(Environment.getExternalStorageDirectory())
        introFragmentRecycleAdapter.addTitle(fileList)
        binding.recycle.layoutManager = LinearLayoutManager(activity)
        binding.recycle.adapter = introFragmentRecycleAdapter
        introFragmentRecycleAdapter.setOnItemClickedListener(object : OnItemClickListener {
            override fun onItemClicked(postion: Int) {
                val bundle = Bundle()
                bundle.putString("pdf", fileList[postion].absolutePath)
                val pdfViewerFragment = PdfViewerFragment()
                pdfViewerFragment.arguments = bundle
                val manager = fragmentManager
                manager?.beginTransaction()?.replace(R.id.container_layout, pdfViewerFragment)
                    ?.addToBackStack(pdfViewerFragment.toString())?.commit()
            }
        })
        binding.searchView.queryHint = "Qidirish"
        binding.searchView.setOnQueryTextListener(object :SearchView.OnQueryTextListener,
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
               introFragmentRecycleAdapter.filter.filter(newText)
                return true
            }

        })
    }

    fun getFile(dir: File) {
        Log.d("DirFile", "getFile: ${dir.absolutePath}")
        val listFiles = dir.listFiles()
        if (listFiles != null && listFiles.isNotEmpty()) {
            for (i in listFiles.indices) {
                if (listFiles[i].isDirectory) {
                    getFile(listFiles[i])
                } else {
                    if (listFiles[i].name.endsWith(".pdf")) {
                        fileList.add(listFiles[i])
                        Log.d("User", "getFile: ${listFiles[i].absolutePath}")
                    }
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {

        inflater.inflate(R.menu.search_menu,menu)
        val search = menu.findItem(R.id.search) as SearchView
        search.setOnQueryTextListener(object :SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                introFragmentRecycleAdapter.filter.filter(query)
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                introFragmentRecycleAdapter.filter.filter(newText)
                return false
            }

        })
        super.onCreateOptionsMenu(menu, inflater)

    }




}







