@file:Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")

package uz.azn.pdfreader.pdfViewerFragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.barteksc.pdfviewer.PDFView
import uz.azn.pdfreader.R
import java.io.File


class PdfViewerFragment() : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_pdf_viewer, container, false)
        val name = arguments!!.getString("pdf")
        val file = File(name)
        val pdfView = view.findViewById<PDFView>(R.id.pdf_view)
        pdfView.fromFile(file)
            .load()

        return view
    }


}