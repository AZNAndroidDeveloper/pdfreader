package uz.azn.pdfreader

import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import uz.azn.pdfreader.databinding.ActivityMainBinding
import uz.azn.pdfreader.splashFragment.SplashFragment
import java.util.jar.Manifest
val READ_EXTERNEL_STORAGE_CODE = 100
class MainActivity : AppCompatActivity() {
    val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        if (ContextCompat.checkSelfPermission(this,android.Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){

            // Does not have a permission
            //we need to ask the permission
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE),
            READ_EXTERNEL_STORAGE_CODE)

        }
        else{
            //Already have permission

            val splashFragment = SplashFragment(this)
            val manager= supportFragmentManager
            manager.beginTransaction().replace(R.id.splash_frame,splashFragment).commit()
        }



    }
}